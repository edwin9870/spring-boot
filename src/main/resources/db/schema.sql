DROP DATABASE IF EXISTS TRAINING_6;
CREATE DATABASE TRAINING_6;
USE TRAINING_6;

create table ADDRESS
(
    ID           int auto_increment
        primary key,
    STREET       varchar(150)                       not null,
    CITY         varchar(50)                         not null,
    STATE        varchar(50)                        not null,
    POSTAL_CODE  varchar(10)                        not null,
    COUNTRY      varchar(70)                        not null,
    CREATED_DATE datetime default CURRENT_TIMESTAMP null,
    UPDATED_DATE datetime default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
);

create table COMPANY
(
    ID           int auto_increment
        primary key,
    NAME         varchar(40)                        not null,
    DESCRIPTION  varchar(255)                       not null,
    CREATED_DATE datetime default CURRENT_TIMESTAMP null,
    UPDATED_DATE datetime default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
);

create table BRANCHES
(
    ID           int auto_increment
        primary key,
    DESCRIPTION  varchar(255)                       not null,
    COMPANY_ID   int                                not null,
    CREATED_DATE datetime default CURRENT_TIMESTAMP null,
    UPDATED_DATE datetime default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP,
    constraint branches_ibfk_1
        foreign key (COMPANY_ID) references COMPANY (ID)
);

create index COMPANY_ID
    on BRANCHES (COMPANY_ID);

create table USER
(
    ID           int auto_increment
        primary key,
    USERNAME     varchar(50)                        not null,
    PASSWORD     varchar(50)                        not null,
    STATUS       varchar(15)                        not null,
    CREATED_DATE datetime default CURRENT_TIMESTAMP null,
    UPDATED_DATE datetime default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP,
    constraint STATUS_CHECK
        check (`STATUS` in ('ACTIVE','DISABLE'))
);

create table PERSON
(
    ID            int auto_increment
        primary key,
    NAME          varchar(70)                        not null,
    PHONE_NUMBER  varchar(25)                        not null,
    EMAIL_ADDRESS varchar(150)                       not null,
    CREATED_DATE  datetime default CURRENT_TIMESTAMP null,
    UPDATED_DATE  datetime default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP,
    USER_ID       int                                null,
    ADDRESS_ID    int                                null,
    constraint person_ibfk_1
        foreign key (USER_ID) references USER (ID),
    constraint person_ibfk_2
        foreign key (ADDRESS_ID) references ADDRESS (ID)
);

create index ADDRESS_ID
    on PERSON (ADDRESS_ID);

create index USER_ID
    on PERSON (USER_ID);

create table WORKER
(
    ID           int auto_increment
        primary key,
    STATUS       varchar(15)                        not null,
    JOIN_DATE    date                               null,
    PERSON_ID    int                                not null,
    COMPANY_ID   int                                not null,
    CREATED_DATE datetime default CURRENT_TIMESTAMP null,
    UPDATED_DATE datetime default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP,
    constraint COMPANY_ID_FRK
        foreign key (COMPANY_ID) references COMPANY (ID),
    constraint worker_ibfk_1
        foreign key (PERSON_ID) references PERSON (ID),
    constraint WORKER_STATUS
        check (`STATUS` in ('ACTIVE','DISABLE'))
);

create index PERSON_ID
    on WORKER (PERSON_ID);

create table WORKER_BRANCH
(
    WORKER_ID int not null,
    BRANCH_ID int not null,
    primary key (WORKER_ID, BRANCH_ID),
    constraint worker_branch_ibfk_1
        foreign key (WORKER_ID) references WORKER (ID),
    constraint worker_branch_ibfk_2
        foreign key (BRANCH_ID) references BRANCHES (ID)
);

create index BRANCH_ID
    on WORKER_BRANCH (BRANCH_ID);

