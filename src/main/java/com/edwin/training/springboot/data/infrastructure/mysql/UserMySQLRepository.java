package com.edwin.training.springboot.data.infrastructure.mysql;

import com.edwin.training.springboot.data.dto.UserDTO;
import com.edwin.training.springboot.data.entity.Person;
import com.edwin.training.springboot.data.entity.User;
import com.edwin.training.springboot.data.infrastructure.mysql.data.PersonDataRepository;
import com.edwin.training.springboot.data.infrastructure.mysql.data.UserDataRepository;
import com.edwin.training.springboot.data.repository.UserRepository;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import org.springframework.stereotype.Repository;

@Repository
public class UserMySQLRepository implements UserRepository {

    private final UserDataRepository userDataRepository;
    private final PersonDataRepository personDataRepository;

    public UserMySQLRepository(UserDataRepository userDataRepository, PersonDataRepository personDataRepository) {
        this.userDataRepository = userDataRepository;
        this.personDataRepository = personDataRepository;
    }

    @Override
    public long create(UserDTO userDTO) {
        User userEntity = new User();
        userEntity.setUserName(userDTO.getUserName());
        userEntity.setPassword(userDTO.getPassword());
        userEntity.setUserName(userDTO.getUserName());
        userEntity.setStatus(userDTO.getStatus());

        userEntity.setCreatedDate(new Date());
        userEntity.setUpdatedDate(new Date());
        if(Objects.nonNull(userDTO.getPersonId())) {
            Optional<Person> personEntity = personDataRepository.findById(userDTO.getPersonId());
            if(personEntity.isEmpty()) {
                throw new RuntimeException("Person reference does not exist");
            }

            userEntity.setPerson(personEntity.get());
        }

        userEntity = userDataRepository.saveAndFlush(userEntity);
        return userEntity.getId();
    }
}
