package com.edwin.training.springboot.data.dto;

import com.edwin.training.springboot.data.entity.Status;
import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WorkerDTO {

    private Long id;
    private Status status;
    private Date joinDate;
    private Long personId;
    private Long companyId;
    private List<Long> branchIds;
    private Date createdDate;
    private Date updatedDate;
}
