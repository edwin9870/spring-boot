package com.edwin.training.springboot.data.dto;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AddressDTO {

    private Long id;
    private String street;
    private String city;
    private String state;
    private String postalCode;
    private String country;
    private Date createdDate;
    private Date updatedDate;

}
