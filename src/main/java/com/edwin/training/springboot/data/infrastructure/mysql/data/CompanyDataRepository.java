package com.edwin.training.springboot.data.infrastructure.mysql.data;

import com.edwin.training.springboot.data.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyDataRepository extends JpaRepository<Company, Long> {

}
