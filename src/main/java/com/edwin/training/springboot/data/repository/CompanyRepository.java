package com.edwin.training.springboot.data.repository;

import com.edwin.training.springboot.data.dto.CompanyDTO;
import com.edwin.training.springboot.data.dto.WorkerDTO;
import com.edwin.training.springboot.data.entity.Status;

public interface CompanyRepository {

    long create(CompanyDTO company);
    long addWorker(WorkerDTO workerDTO);
    void changeWorkerStatus(long companyId, long workerId, Status status);
    void delete(long companyId);
}
