package com.edwin.training.springboot.data.dto;

import com.edwin.training.springboot.data.entity.Status;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserDTO {
    private Long id;
    private String userName;
    private String password;
    private Status status;
    private Long personId;
    private Date createdDate;
    private Date updatedDate;
}
