package com.edwin.training.springboot.data.repository;

import com.edwin.training.springboot.data.dto.UserDTO;

public interface UserRepository {

    long create(UserDTO userDTO);

}
