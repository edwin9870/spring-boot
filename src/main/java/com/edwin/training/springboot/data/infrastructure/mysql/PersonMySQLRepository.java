package com.edwin.training.springboot.data.infrastructure.mysql;

import com.edwin.training.springboot.data.dto.PersonDTO;
import com.edwin.training.springboot.data.entity.Address;
import com.edwin.training.springboot.data.entity.Person;
import com.edwin.training.springboot.data.entity.User;
import com.edwin.training.springboot.data.infrastructure.mysql.data.PersonDataRepository;
import com.edwin.training.springboot.data.infrastructure.mysql.data.UserDataRepository;
import com.edwin.training.springboot.data.repository.PersonRepository;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import org.springframework.stereotype.Repository;

@Repository
public class PersonMySQLRepository implements PersonRepository {

    private final UserDataRepository userDataRepository;
    private final PersonDataRepository personDataRepository;

    public PersonMySQLRepository(UserDataRepository userDataRepository, PersonDataRepository personDataRepository) {
        this.userDataRepository = userDataRepository;
        this.personDataRepository = personDataRepository;
    }

    @Override
    public long create(PersonDTO personDTO) {
        Person personEntity = new Person();

        personEntity.setName(personDTO.getName());
        personEntity.setPhoneNumber(personDTO.getPhoneNumber());
        personEntity.setEmailAddress(personDTO.getEmailAddress());
        personEntity.setCreatedDate(new Date());
        personEntity.setUpdatedDate(new Date());
        if (Objects.nonNull(personDTO.getUserId())) {
            Optional<User> userEntity = userDataRepository.findById(personDTO.getUserId());
            if (userEntity.isEmpty()) {
                throw new RuntimeException("Person reference does not exist");
            }
            personEntity.setUser(userEntity.get());
        }

        if (Objects.nonNull(personDTO.getAddress())) {
            personEntity.setAddress(Address.builder()
                    .street(personDTO.getAddress().getStreet())
                    .city(personDTO.getAddress().getCity())
                    .state(personDTO.getAddress().getState())
                    .postalCode(personDTO.getAddress().getPostalCode())
                    .country(personDTO.getAddress().getCountry())
                    .createdDate(new Date())
                    .updatedDate(new Date())
                    .build());
        }

        personEntity = personDataRepository.saveAndFlush(personEntity);
        return personEntity.getId();
    }
}
