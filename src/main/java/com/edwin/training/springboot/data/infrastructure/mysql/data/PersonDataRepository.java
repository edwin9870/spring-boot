package com.edwin.training.springboot.data.infrastructure.mysql.data;

import com.edwin.training.springboot.data.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonDataRepository extends JpaRepository<Person, Long> {

}
