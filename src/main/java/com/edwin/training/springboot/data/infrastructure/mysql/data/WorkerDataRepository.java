package com.edwin.training.springboot.data.infrastructure.mysql.data;

import com.edwin.training.springboot.data.entity.Worker;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkerDataRepository extends JpaRepository<Worker, Long> {

    Worker findWorkerByIdAndCompanyId(Long workerId, Long companyId);

}
