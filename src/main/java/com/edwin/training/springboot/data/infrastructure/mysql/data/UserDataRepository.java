package com.edwin.training.springboot.data.infrastructure.mysql.data;

import com.edwin.training.springboot.data.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDataRepository extends JpaRepository<User, Long> {

}
