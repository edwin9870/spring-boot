package com.edwin.training.springboot.data.repository;

import com.edwin.training.springboot.data.dto.PersonDTO;

public interface PersonRepository {

    long create(PersonDTO personDTO);

}
