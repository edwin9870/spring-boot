package com.edwin.training.springboot.data.infrastructure.mysql;

import com.edwin.training.springboot.data.dto.CompanyDTO;
import com.edwin.training.springboot.data.dto.WorkerDTO;
import com.edwin.training.springboot.data.entity.Branches;
import com.edwin.training.springboot.data.entity.Company;
import com.edwin.training.springboot.data.entity.Status;
import com.edwin.training.springboot.data.entity.Worker;
import com.edwin.training.springboot.data.infrastructure.mysql.data.BranchesDataRepository;
import com.edwin.training.springboot.data.infrastructure.mysql.data.CompanyDataRepository;
import com.edwin.training.springboot.data.infrastructure.mysql.data.PersonDataRepository;
import com.edwin.training.springboot.data.infrastructure.mysql.data.WorkerDataRepository;
import com.edwin.training.springboot.data.repository.CompanyRepository;
import java.util.ArrayList;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

@Repository
@Slf4j
public class CompanyMySQLRepository implements CompanyRepository {

    private final CompanyDataRepository companyDataRepository;
    private final WorkerDataRepository workerDataRepository;
    private final PersonDataRepository personDataRepository;
    private final BranchesDataRepository branchesDataRepository;

    public CompanyMySQLRepository(CompanyDataRepository companyDataRepository,
            WorkerDataRepository workerDataRepository, PersonDataRepository personDataRepository,
            BranchesDataRepository branchesDataRepository) {
        this.companyDataRepository = companyDataRepository;
        this.workerDataRepository = workerDataRepository;
        this.personDataRepository = personDataRepository;
        this.branchesDataRepository = branchesDataRepository;
    }

    @Override
    public long create(CompanyDTO companyDTO) {
        final Company companyEntity = new Company();
        companyEntity.setName(companyDTO.getName());
        companyEntity.setDescription(companyDTO.getDescription());
        companyEntity.setName(companyDTO.getName());
        companyEntity.setBranchesList(new ArrayList<>());
        companyEntity.setCreatedDate(companyDTO.getCreatedDate());
        companyEntity.setUpdatedDate(companyDTO.getUpdatedDate());

        Optional.ofNullable(companyDTO.getBranches()).orElseGet(ArrayList::new).forEach(e -> companyEntity.getBranchesList().add(Branches.builder()
                .description(e.getDescription())
                .createdDate(e.getCreatedDate())
                .updatedDate(e.getUpdatedDate())
                .company(companyEntity)
                .build()));

        Company returnedCompany = companyDataRepository.saveAndFlush(companyEntity);

        return returnedCompany.getId();
    }

    @Override
    public long addWorker(WorkerDTO workerDTO) {
        Worker workerEntity = Worker.builder()
                .status(Status.ACTIVE)
                .joinDate(workerDTO.getJoinDate())
                .createdDate(workerDTO.getCreatedDate())
                .updatedDate(workerDTO.getUpdatedDate())
                .company(companyDataRepository.findById(workerDTO.getCompanyId()).get())
                .person(personDataRepository.findById(workerDTO.getPersonId()).get())
                .branchesList(new ArrayList<>())
                .build();

        Optional.ofNullable(workerDTO.getBranchIds()).orElseGet(ArrayList::new).forEach(e -> {
            Branches branch = branchesDataRepository.findById(e).get();
            branch.getWorkers().add(workerEntity);
            workerEntity.getBranchesList().add(branch);
        });

        log.info("Worker entity to persist: {}, with branches: {}", workerEntity, workerEntity.getBranchesList());

        return workerDataRepository.saveAndFlush(workerEntity).getId();
    }

    @Override
    public void changeWorkerStatus(long companyId, long workerId, Status status) {
        Worker worker = workerDataRepository.findWorkerByIdAndCompanyId(workerId, companyId);
        worker.setStatus(status);
        workerDataRepository.save(worker);
    }

    @Override
    public void delete(long companyId) {
        companyDataRepository.delete(companyDataRepository.getById(companyId));
    }
}
