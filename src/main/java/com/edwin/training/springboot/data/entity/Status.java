package com.edwin.training.springboot.data.entity;

public enum Status {
    ACTIVE, DISABLE;
}
