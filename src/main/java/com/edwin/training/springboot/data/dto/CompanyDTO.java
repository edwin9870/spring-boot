package com.edwin.training.springboot.data.dto;

import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CompanyDTO {

    private Long id;
    private String name;
    private String description;
    private List<BranchDTO> branches;
    private Date createdDate;
    private Date updatedDate;

}
