package com.edwin.training.springboot.data.dto;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class PersonDTO {

    private Long id;
    private String name;
    private String phoneNumber;
    private String emailAddress;
    private Long userId;
    private AddressDTO address;
    private Date createdDate;
    private Date updatedDate;
}
