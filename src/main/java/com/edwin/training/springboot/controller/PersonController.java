package com.edwin.training.springboot.controller;

import com.edwin.training.springboot.controller.dto.PersonCreationRequestDTO;
import com.edwin.training.springboot.controller.dto.PersonCreationResponseDTO;

public interface PersonController {

    PersonCreationResponseDTO create(PersonCreationRequestDTO personCreationRequestDTO);
}
