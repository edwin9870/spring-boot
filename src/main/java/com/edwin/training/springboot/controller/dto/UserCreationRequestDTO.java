package com.edwin.training.springboot.controller.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserCreationRequestDTO {
    private String userName;
    private String password;
    private Long personId;
}
