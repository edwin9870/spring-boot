package com.edwin.training.springboot.controller;

import com.edwin.training.springboot.controller.dto.UserCreationRequestDTO;
import com.edwin.training.springboot.controller.dto.UserCreationResponseDTO;
import com.edwin.training.springboot.data.dto.UserDTO;
import com.edwin.training.springboot.data.entity.Status;
import com.edwin.training.springboot.service.UserService;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/user")
@Slf4j
public class UserControllerImpl implements UserController {

    private final UserService userService;

    public UserControllerImpl(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public @ResponseBody UserCreationResponseDTO create(@RequestBody UserCreationRequestDTO userCreationRequestDTO) {
        log.info("Hi, userCreationRequestDTO: {}", userCreationRequestDTO);

        UserDTO userDTO = new UserDTO();
        userDTO.setUserName(userCreationRequestDTO.getUserName());
        userDTO.setPassword(userCreationRequestDTO.getPassword());
        userDTO.setStatus(Status.ACTIVE);
        if(Objects.nonNull(userCreationRequestDTO.getPersonId())) {
            userDTO.setPersonId(userCreationRequestDTO.getPersonId());
        }
        long userId = userService.create(userDTO);
        return new UserCreationResponseDTO(userId);
    }

}
