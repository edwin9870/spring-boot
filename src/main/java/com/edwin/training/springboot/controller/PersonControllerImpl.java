package com.edwin.training.springboot.controller;

import com.edwin.training.springboot.controller.dto.AddressRequestDTO;
import com.edwin.training.springboot.controller.dto.PersonCreationRequestDTO;
import com.edwin.training.springboot.controller.dto.PersonCreationResponseDTO;
import com.edwin.training.springboot.data.dto.AddressDTO;
import com.edwin.training.springboot.data.dto.PersonDTO;
import com.edwin.training.springboot.service.PersonService;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/person")
@Slf4j
public class PersonControllerImpl implements PersonController {

    private final PersonService personService;

    public PersonControllerImpl(PersonService personService) {
        this.personService = personService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public @ResponseBody PersonCreationResponseDTO create(
            @RequestBody PersonCreationRequestDTO personCreationRequestDTO) {
        log.info("Hi, personCreationRequestDTO: {}", personCreationRequestDTO);
        PersonDTO personDTO = PersonDTO.builder()
                .name(personCreationRequestDTO.getName())
                .phoneNumber(personCreationRequestDTO.getPhoneNumber())
                .emailAddress(personCreationRequestDTO.getEmailAddress())
                .userId(personCreationRequestDTO.getUserId())
                .build();

        if(Objects.nonNull(personCreationRequestDTO.getAddresses())) {
            personDTO.setAddress(toAddressDTO(personCreationRequestDTO.getAddresses()));
        }

        long personId = personService.create(personDTO);

        return new PersonCreationResponseDTO(personId);

    }

    private AddressDTO toAddressDTO(AddressRequestDTO personCreationRequestDTO) {
        return AddressDTO.builder()
                .street(personCreationRequestDTO.getStreet())
                .state(personCreationRequestDTO.getState())
                .city(personCreationRequestDTO.getCity())
                .postalCode(personCreationRequestDTO.getPostalCode())
                .country(personCreationRequestDTO.getCountry())
                .build();
    }

}
