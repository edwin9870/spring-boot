package com.edwin.training.springboot.controller;

import com.edwin.training.springboot.controller.dto.UserCreationRequestDTO;
import com.edwin.training.springboot.controller.dto.UserCreationResponseDTO;

public interface UserController {

    UserCreationResponseDTO create(UserCreationRequestDTO userCreationRequestDTO);
}
