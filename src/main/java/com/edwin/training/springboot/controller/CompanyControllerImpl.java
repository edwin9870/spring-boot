package com.edwin.training.springboot.controller;

import com.edwin.training.springboot.controller.dto.ChangeWorkerStatusRequestDTO;
import com.edwin.training.springboot.controller.dto.CompanyCreationRequestDTO;
import com.edwin.training.springboot.controller.dto.CompanyCreationResponseDTO;
import com.edwin.training.springboot.controller.dto.WorkerCreationRequestDTO;
import com.edwin.training.springboot.controller.dto.WorkerCreationResponseDTO;
import com.edwin.training.springboot.data.dto.BranchDTO;
import com.edwin.training.springboot.data.dto.CompanyDTO;
import com.edwin.training.springboot.data.dto.WorkerDTO;
import com.edwin.training.springboot.data.entity.Status;
import com.edwin.training.springboot.service.CompanyService;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/company")
@Slf4j
public class CompanyControllerImpl implements CompanyController {

    private final CompanyService companyService;

    public CompanyControllerImpl(CompanyService companyService) {
        this.companyService = companyService;
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public @ResponseBody CompanyCreationResponseDTO create(
            @RequestBody CompanyCreationRequestDTO companyCreationRequestDTO) {
        log.info("request {}", companyCreationRequestDTO);

        CompanyDTO companyDTO = CompanyDTO.builder()
                .name(companyCreationRequestDTO.getName())
                .description(companyCreationRequestDTO.getDescription())
                .createdDate(new Date())
                .updatedDate(new Date())
                .branches(new ArrayList<>())
                .build();

        if (companyCreationRequestDTO.getBranches() != null) {
            companyCreationRequestDTO.getBranches().forEach(e -> {
                companyDTO.getBranches().add(BranchDTO.builder()
                        .description(e.getDescription())
                        .createdDate(new Date())
                        .updatedDate(new Date())
                        .build());
            });
        }

        long companyId = companyService.create(companyDTO);
        return new CompanyCreationResponseDTO(companyId);
    }

    @PostMapping("/{companyId}/worker")
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public @ResponseBody WorkerCreationResponseDTO createWorker(
            @PathVariable("companyId") long companyId,
            @RequestBody WorkerCreationRequestDTO workerCreationRequestDTO) {
        log.info("companyId: {}, workerCreationRequestDTO: {}", companyId, workerCreationRequestDTO);

        WorkerDTO workerDTO = WorkerDTO.builder()
                .status(Status.ACTIVE)
                .joinDate(new Date())
                .personId(workerCreationRequestDTO.getPersonId())
                .companyId(companyId)
                .createdDate(new Date())
                .updatedDate(new Date())
                .branchIds(new ArrayList<>())
                .build();

        Optional.ofNullable(workerCreationRequestDTO.getBranchIds()).orElseGet(ArrayList::new).forEach(e -> workerDTO.getBranchIds().add(e));

        long workerId = companyService.addWorker(workerDTO);
        return new WorkerCreationResponseDTO(workerId);
    }

    @PatchMapping("/{companyId}/worker/{workerId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public void updateWorkerStatus(
            @PathVariable("companyId") long companyId,
            @PathVariable("workerId") long workerId,
            @RequestBody ChangeWorkerStatusRequestDTO changeWorkerStatusRequestDTO) {
        log.info("companyId: {}, workerId: {}, changeWorkerStatusRequestDTO: {}", companyId, workerId, changeWorkerStatusRequestDTO);
        companyService.changeWorkerStatus(companyId, workerId, Status.valueOf(changeWorkerStatusRequestDTO.getStatus()));

    }

    @DeleteMapping("/{companyId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public void deleteCompany(@PathVariable("companyId") long companyId) {
        companyService.delete(companyId);
    }

}
