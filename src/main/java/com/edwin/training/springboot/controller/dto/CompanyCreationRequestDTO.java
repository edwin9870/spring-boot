package com.edwin.training.springboot.controller.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompanyCreationRequestDTO {
    private String name;
    private String description;
    private List<BranchRequestDTO> branches;

}
