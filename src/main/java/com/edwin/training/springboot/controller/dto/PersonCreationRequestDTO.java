package com.edwin.training.springboot.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PersonCreationRequestDTO {

    private String name;
    private String phoneNumber;
    private String emailAddress;
    private AddressRequestDTO addresses;
    private Long userId;
}
