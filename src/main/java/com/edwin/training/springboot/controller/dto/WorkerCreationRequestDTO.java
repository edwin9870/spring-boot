package com.edwin.training.springboot.controller.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WorkerCreationRequestDTO {
    public List<Long> branchIds;
    private Long personId;

}
