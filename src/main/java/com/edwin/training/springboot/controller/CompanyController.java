package com.edwin.training.springboot.controller;

import com.edwin.training.springboot.controller.dto.ChangeWorkerStatusRequestDTO;
import com.edwin.training.springboot.controller.dto.CompanyCreationRequestDTO;
import com.edwin.training.springboot.controller.dto.CompanyCreationResponseDTO;
import com.edwin.training.springboot.controller.dto.WorkerCreationRequestDTO;
import com.edwin.training.springboot.controller.dto.WorkerCreationResponseDTO;
import org.springframework.web.bind.annotation.PathVariable;

public interface CompanyController {

    CompanyCreationResponseDTO create(CompanyCreationRequestDTO companyCreationRequestDTO);

    WorkerCreationResponseDTO createWorker(long companyId, WorkerCreationRequestDTO workerCreationRequestDTO);

    void updateWorkerStatus(long companyId, long workerId, ChangeWorkerStatusRequestDTO changeWorkerStatusRequestDTO);

    void deleteCompany(long companyId);
}
