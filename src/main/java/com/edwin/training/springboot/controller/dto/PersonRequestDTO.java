package com.edwin.training.springboot.controller.dto;

import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PersonRequestDTO {

    private String name;
    private String phoneNumber;
    private String emailAddress;
    private List<AddressRequestDTO> addresses;
}
