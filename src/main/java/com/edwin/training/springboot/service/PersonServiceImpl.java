package com.edwin.training.springboot.service;

import com.edwin.training.springboot.data.dto.PersonDTO;
import com.edwin.training.springboot.data.repository.PersonRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public long create(PersonDTO personDTO) {
        return personRepository.create(personDTO);
    }
}
