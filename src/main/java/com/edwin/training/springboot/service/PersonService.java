package com.edwin.training.springboot.service;

import com.edwin.training.springboot.data.dto.PersonDTO;

public interface PersonService {

    long create(PersonDTO personDTO);

}
