package com.edwin.training.springboot.service;

import com.edwin.training.springboot.data.dto.CompanyDTO;
import com.edwin.training.springboot.data.dto.WorkerDTO;
import com.edwin.training.springboot.data.entity.Status;

public interface CompanyService {

    long create(CompanyDTO companyDTO);
    long addWorker(WorkerDTO workerDTO);
    void changeWorkerStatus(long companyId, long workerId, Status status);
    void delete(long companyId);
}
