package com.edwin.training.springboot.service;

import com.edwin.training.springboot.data.dto.UserDTO;
import com.edwin.training.springboot.data.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public long create(UserDTO userDTO) {
        return userRepository.create(userDTO);
    }
}
