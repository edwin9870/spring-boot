package com.edwin.training.springboot.service;

import com.edwin.training.springboot.data.dto.CompanyDTO;
import com.edwin.training.springboot.data.dto.WorkerDTO;
import com.edwin.training.springboot.data.entity.Status;
import com.edwin.training.springboot.data.repository.CompanyRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
public class CompanyServiceImpl implements CompanyService {

    private final CompanyRepository companyRepository;

    public CompanyServiceImpl(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }


    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public long create(CompanyDTO companyDTO) {
        return companyRepository.create(companyDTO);
    }

    @Override
    public long addWorker(WorkerDTO workerDTO) {
        return companyRepository.addWorker(workerDTO);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void changeWorkerStatus(long companyId, long workerId, Status status) {
        companyRepository.changeWorkerStatus(companyId, workerId, status);

    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void delete(long companyId) {
        companyRepository.delete(companyId);

    }
}
