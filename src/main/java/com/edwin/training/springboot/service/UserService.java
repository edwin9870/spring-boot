package com.edwin.training.springboot.service;

import com.edwin.training.springboot.data.dto.UserDTO;

public interface UserService {

    long create(UserDTO userDTO);

}
